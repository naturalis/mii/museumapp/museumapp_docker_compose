version: '3.4'

services:

  traefik:
    image: traefik:${TRAEFIK_VERSION:?err}
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_traefik"
    restart: ${CONTAINER_RESTART:-unless-stopped}
    depends_on:
      - docker-socket-proxy
      - image_selector
      - image_squares
      - pipeline
      - api
      - minio
    networks:
      - docker-socket-proxy
      - web
    ports:
      - "${HOST_IP:-0.0.0.0}:80:80"
      - "${HOST_IP:-0.0.0.0}:443:443"
      - "127.0.0.1:${TRAEFIK_API_PORT:-8079}:8080"
    volumes:
      - "./letsencrypt:/letsencrypt"
    command:
      - "--global.sendAnonymousUsage=false"
      - "--log.level=${TRAEFIK_LOG_LEVEL:-ERROR}"
      - "--api=${TRAEFIK_API_ENABLED:-false}"
      - "--api.insecure=true"
      - "--entrypoints.web.address=:80"
      - "--entrypoints.web.http.redirections.entrypoint.to=websecure"
      - "--entrypoints.web.http.redirections.entrypoint.scheme=https"
      - "--entrypoints.web.http.redirections.entrypoint.permanent=true"
      - "--entrypoints.websecure.address=:443"
      - "--certificatesresolvers.route53.acme.dnschallenge=true"
      - "--certificatesresolvers.route53.acme.dnschallenge.provider=route53"
      - "--certificatesresolvers.route53.acme.storage=/letsencrypt/route53.json"
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--providers.docker.endpoint=tcp://docker-socket-proxy:2375"
      - "--providers.docker.network=docker-socket-proxy"
    env_file:
      - .env

  docker-socket-proxy:
    image: tecnativa/docker-socket-proxy:${DOCKERPROXY_VERSION:-latest}
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_docker-socket-proxy"
    restart: unless-stopped
    networks:
      - docker-socket-proxy
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro,delegated
    environment:
      CONTAINERS: 1

  elastic:
    restart: on-failure:5
    image: docker.elastic.co/elasticsearch/elasticsearch:7.16.2
    networks:
      - web
    environment:
      - discovery.type=single-node
    ports:
      - 9200:9200
    volumes:
      - "${BASE_PATH:-/data}/pipeline/es-data:/usr/share/elasticsearch/data"

  mysql:
    image: "mysql:5.7"
    restart: unless-stopped
    environment:
      MYSQL_ROOT_PASSWORD: ${MYSQL_PASSWORD}
    networks:
      - web
    ports:
        - "${MYSQL_EXTERNAL_PORT:-3307}:3306"
    volumes:
      - "${BASE_PATH:-/data}/pipeline/mysql/initdb:/docker-entrypoint-initdb.d"
      - "${BASE_PATH:-/data}/pipeline/mysql/db:/var/lib/mysql"
      - "${BASE_PATH:-/data}/pipeline/mysql/mysqllog:/var/log/mysql"
      - "${BASE_PATH:-/data}/pipeline/mysql/mysql-files:/var/lib/mysql-files/"
    env_file:
      - .env

  minio:
    image: minio/minio:RELEASE.2019-09-11T19-53-16Z
    restart: unless-stopped
    command: server /export
    environment:
      MINIO_ACCESS_KEY: ${MINIO_USER}
      MINIO_SECRET_KEY: ${MINIO_PASS}
    volumes:
      - "${MINIO_DATA_DIR:-/data/pipeline/minio}:/export"
    networks:
      - web
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.museumappminio.entrypoints=websecure"
      - "traefik.http.routers.museumappminio.rule=${MINIO_FQDN:-Host(`minio-museumapp.naturalis.nl`)}"
      - "traefik.http.routers.museumappminio.tls.certresolver=route53"

  bootstrap:
    image: registry.gitlab.com/naturalis/mii/museumapp/museumapp_bootstrap:1.4
    volumes:
      - "${BASE_PATH:-/data}/pipeline:/data"
      - "${BASE_PATH:-/data}/pipeline:/data/log"
      - "${BASE_PATH:-/data}/pipeline/leenobject_images:/data/leenobject_images"
      - "${BASE_PATH:-/data}/pipeline/stubs:/data/stubs"
      - "${BASE_PATH:-/data}/pipeline/image_selector:/data/image_selector"
      - "${BASE_PATH:-/data}/pipeline/image_squares:/data/image_squares"
      - "${BASE_PATH:-/data}/pipeline/document_hashes:/data/document_hashes"
      - "${BASE_PATH:-/data}/pipeline/management_data:/data/management_data"
      - "${BASE_PATH:-/data}/pipeline/squared_images:/data/squared_images"
      - "${BASE_PATH:-/data}/pipeline/es-data:/usr/share/elasticsearch/data"
      - "${BASE_PATH:-/data}/pipeline/documents/preview:${JSON_PREVIEW_PATH}"
      - "${BASE_PATH:-/data}/pipeline/documents/publish:${JSON_PUBLISH_PATH}"
      - "${BASE_PATH:-/data}/pipeline/documents/load:${JSON_LOAD_PATH}"
      # - "${BASE_PATH:-/data}/pipeline/export:${TRANSLATOR_EXPORT_PATH}"
    env_file:
      - .env
    depends_on:
      - "mysql"

  crs_harvester:
    image: registry.gitlab.com/naturalis/mii/museumapp/museumapp_crs_harvester:1.3
    networks:
      - web
    volumes:
      - "${BASE_PATH:-/data}/pipeline:/data"
    env_file:
      - .env
    links:
      - mysql:mysql

  image_selector:
    image: registry.gitlab.com/naturalis/mii/museumapp/museumapp_image_selector:1.2
    networks:
      - web
    volumes:
      - "${BASE_PATH:-/data}/pipeline/image_selector:/data/image_selector"
    env_file:
      - .env
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.museumappselector.entrypoints=websecure"
      - "traefik.http.routers.museumappselector.rule=${SELECTOR_FQDN:-Host(`selector-museumapp.naturalis.nl`)}"
      - "traefik.http.routers.museumappselector.tls.certresolver=route53"
      - "traefik.http.middlewares.squares-auth.basicauth.realm=My Realm"
      - "traefik.http.middlewares.squares-auth.basicauth.users=${IMAGE_EDIT_AUTH_USER}:$$apr1$$vsuYeDki$$eP3dDXWOImmneC6A93VKa1"
      - "traefik.http.routers.museumappselector.middlewares=squares-auth"
    depends_on:
      - "bootstrap"

  image_squares:
    image: registry.gitlab.com/naturalis/mii/museumapp/museumapp_image_squares/image_squares:1.3
    networks:
      - web
    volumes:
      - "${BASE_PATH:-/data}/pipeline/image_squares:/data/image_squares"
    env_file:
      - .env
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.museumappsquares.entrypoints=websecure"
      - "traefik.http.routers.museumappsquares.rule=${SQUARES_FQDN:-Host(`squares-museumapp.naturalis.nl`)}"
      - "traefik.http.routers.museumappsquares.tls.certresolver=route53"
      - "traefik.http.middlewares.squares-auth.basicauth.realm=My Realm"
      - "traefik.http.middlewares.squares-auth.basicauth.users=${IMAGE_EDIT_AUTH_USER}:$$apr1$$vsuYeDki$$eP3dDXWOImmneC6A93VKa1"
      - "traefik.http.routers.museumappsquares.middlewares=squares-auth"
    depends_on:
      - "bootstrap"

  square_maker:
    image: registry.gitlab.com/naturalis/mii/museumapp/museumapp_image_squares/image_square_maker:1.5
    networks:
      - web
    volumes:
      - "${BASE_PATH:-/data}/pipeline/image_squares:/data/image_squares"
      - "${BASE_PATH:-/data}/pipeline/squared_images:/data/squared_images"
    env_file:
      - .env

  nba_harvester:
    image: registry.gitlab.com/naturalis/mii/museumapp/museumapp_nba_harvester:1.3
    networks:
      - web
    env_file:
      - .env
    volumes:
      - "${BASE_PATH:-/data}/pipeline:/data"
    links:
      - mysql:mysql

  pipeline:
    image: registry.gitlab.com/naturalis/mii/museumapp/museumapp_pipeline:1.9.1
    networks:
      - web
    volumes:
      - "${BASE_PATH:-/data}/pipeline:/data"
      - "${BASE_PATH:-/data}/pipeline/documents:/data/documents"
      - "${BASE_PATH:-/data}/pipeline/log:/data/log"
      - "${BASE_PATH:-/data}/pipeline/queue:/data/queue"
    env_file:
      - .env
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.museumappipeline.entrypoints=websecure"
      - "traefik.http.routers.museumappipeline.rule=${PIPELINE_FQDN:-Host(`pipeline-museumapp.naturalis.nl`)}"
      - "traefik.http.routers.museumappipeline.tls.certresolver=route53"
    depends_on:
      - "bootstrap"

  api:
    restart: on-failure:5
    image: registry.gitlab.com/naturalis/mii/museumapp/museumapp_pipeline_api/api:1.2
    networks:
      - web
    ports:
      - 5000
    env_file:
      - .env
    volumes:
      - "${BASE_PATH:-/data}/pipeline:/data"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.museumappapi.entrypoints=websecure"
      - "traefik.http.routers.museumappapi.rule=${API_FQDN:-Host(`museumapp.naturalis.nl`)}"
      - "traefik.http.routers.museumappapi.tls.certresolver=route53"
    links:
      - elastic:elastic
    depends_on:
      - "bootstrap"

  api_control:
    image: registry.gitlab.com/naturalis/mii/museumapp/museumapp_pipeline_api/api_control:1.2
    networks:
      - web
    env_file:
      - .env
    volumes:
      - "${BASE_PATH:-/data}/pipeline:/data"
    links:
      - elastic:elastic
    depends_on:
      - "bootstrap"

  document_loader:
    image: registry.gitlab.com/naturalis/mii/museumapp/museumapp_pipeline_api/document_loader:1.2
    networks:
      - web
    env_file:
      - .env
    volumes:
      - "${BASE_PATH:-/data}/pipeline:/data"
    links:
      - elastic:elastic
    depends_on:
      - "bootstrap"

  reaper:
    image: registry.gitlab.com/naturalis/mii/museumapp/museumapp_reaper:1.3
    networks:
      - web
    env_file:
      - .env
    volumes:
      - "${BASE_PATH:-/data}/pipeline:/data"
    links:
      - mysql:mysql

  translator:
    image: registry.gitlab.com/naturalis/mii/museumapp/museumapp_translator:1.1
    networks:
      - web
    volumes:
      - "${BASE_PATH:-/data}/pipeline:/data"
    env_file:
      - .env
    depends_on:
      - "bootstrap"

networks:
  web:
    internal: false
  docker-socket-proxy:
    internal: true
