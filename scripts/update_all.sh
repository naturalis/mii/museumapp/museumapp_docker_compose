#!/bin/bash

# file is assumed to be run from a subdir of the directory holding the docker-compose.yml file

DOCKER_COMPOSE=$(whereis docker-compose | cut -d ' ' -f2)
CRON_LOG=$(grep CRON_LOG ../.env | cut -d '=' -f2)
CRON_MONITOR=$(grep CRON_MONITOR ../.env | cut -d '=' -f2)
JOB_QUEUE_PATH_HOST=$(grep JOB_QUEUE_PATH_HOST ../.env | cut -d '=' -f2)
LOCK_FILE=${JOB_QUEUE_PATH_HOST}.update_lock

# Ruud: release lock if older than 12 h
LOCK_RELEASE=$(("60*60*12"))
if [ -f "$LOCK_FILE" ]; then
	LAST_RUN_TIMESTAMP=$(date -d "$(cat $LOCK_FILE)" +%s)
	if [ $(($LAST_RUN_TIMESTAMP + $LOCK_RELEASE)) -lt $(date +%s) ]; then
		echo $(date "+%Y-%m-%d %H:%M:%S")": daily script lock file outdated, removed" >> $CRON_MONITOR
       	rm $LOCK_FILE
    else
		echo "update in progress (lock file exists); exiting"
		echo $(date "+%Y-%m-%d %H:%M:%S")": daily script cancelled, lock file exists" >> $CRON_MONITOR
		exit
	fi
fi

date > $LOCK_FILE

echo "updating all sources"
echo $(date "+%Y-%m-%d %H:%M:%S")": starting daily update all script" >> $CRON_MONITOR

cd ..

$DOCKER_COMPOSE run --rm square_maker ./make_squares.sh >> $CRON_LOG
$DOCKER_COMPOSE run --rm reaper php ./public/run.php --source=natuurwijzer >> $CRON_LOG
$DOCKER_COMPOSE run --rm reaper php ./public/run.php --source=tentoonstelling >> $CRON_LOG
$DOCKER_COMPOSE run --rm reaper php ./public/run.php --source=topstukken >> $CRON_LOG
$DOCKER_COMPOSE run --rm nba_harvester php run.php --source=leenobjecten >> $CRON_LOG
$DOCKER_COMPOSE run --rm reaper php ./public/run.php --source=ttik >> $CRON_LOG
$DOCKER_COMPOSE run --rm nba_harvester php run.php --source=nba >> $CRON_LOG
$DOCKER_COMPOSE run --rm nba_harvester php run.php --source=favourites >> $CRON_LOG
$DOCKER_COMPOSE run --rm nba_harvester php run.php --source=taxa_no_objects >> $CRON_LOG
$DOCKER_COMPOSE run --rm pipeline php run.php --generate-files=0 >> $CRON_LOG
$DOCKER_COMPOSE run --rm crs_harvester php run.php >> $CRON_LOG
$DOCKER_COMPOSE run --rm nba_harvester php run.php --source=maps >> $CRON_LOG
$DOCKER_COMPOSE run --rm nba_harvester php run.php --source=iucn >> $CRON_LOG
$DOCKER_COMPOSE run --rm pipeline php run.php >> $CRON_LOG

echo $(date "+%Y-%m-%d %H:%M:%S")": daily update all script ready" >> $CRON_MONITOR

rm $LOCK_FILE

